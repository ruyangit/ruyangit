$(function () {

  'use strict'

  $('[data-toggle="tooltip"]').tooltip();

  // set active component from list to show in desktop view by default
  //if(window.matchMedia('(min-width: 992px)').matches) {
  //$('.component-list .media:first-of-type').addClass('active');
  //}


  const componentSidebar = new PerfectScrollbar('.component-sidebar-body', {
    suppressScrollX: true
  });

  new PerfectScrollbar('.component-content-body', {
    suppressScrollX: true
  });

  new PerfectScrollbar('.component-content-sidebar', {
    suppressScrollX: true
  });

  $('.component-navleft .nav-link').on('shown.bs.tab', function (e) {
    componentSidebar.update()
  })

  // UI INTERACTION
  $('.component-list .media a').on('click', function (e) {
    if (window.matchMedia('(max-width: 991px)').matches &&
      e.currentTarget.href === window.location.href) {
      e.preventDefault();
    }

    $('.component-list .media').removeClass('active');
    $(this).parent().addClass('active');

    // showing component information when clicking one of the list
    // for mobile interaction only
    if (window.matchMedia('(max-width: 991px)').matches) {
      $('body').addClass('component-content-show');
      $('body').removeClass('component-content-visible');

      $('#mainMenuOpen').addClass('d-none');
      $('#componentContentHide').removeClass('d-none');
    }
  })

  // going back to component list
  // for mobile interaction only
  $('#componentContentHide').on('click touch', function (e) {
    e.preventDefault();

    $('body').removeClass('component-content-show component-options-show');
    $('body').addClass('component-content-visible');

    $('#mainMenuOpen').removeClass('d-none');
    $(this).addClass('d-none');
  });

  $('#componentOptions').on('click', function (e) {
    e.preventDefault();
    $('body').toggleClass('component-options-show');
  })

  $(window).resize(function () {
    $('body').removeClass('component-options-show');
  })

})
